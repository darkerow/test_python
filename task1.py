__author__ = 'darkerow'

class Keeper:
    def __init__(self):
        self.instances = []
        self.index = 0
    def __iter__(self):
        self.index = 0
        return self
    def __next__(self):
        try:
            result = self.instances[self.index]
        except IndexError:
            raise StopIteration
        self.index += 1
        return result
    def append(self, child):
        self.instances.append(child)

class Keeper2:
    def __init__(self):
        self.instances = []
    def append(self, child):
        self.instances.append(child)
    def list_instances(self):
        return  self.instances
    def append(self, child):
        self.instances.append(child)

#with iterators
a = Keeper()
a.append(Keeper())
a.append(Keeper())
for i in a:
    print(i)

#without iterators
b = Keeper2()
b.append(Keeper())
b.append(Keeper())
for i in b.list_instances():
    print(i)