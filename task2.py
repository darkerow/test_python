__author__ = 'darkerow'

arr = [1,2,3,4,5]
#возвращаем объект генератора
#(i for i in arr)
#возвращает список
#[i for i in arr]

print([i for i in arr])
print((i for i in arr))


# Напишите генератор следующего вида
# gen = reverse_gen([1, 2, 3, 4])
# >>> it.next()

class Reverse:
    def __init__(self, data):
        self.data = data
        self.index = len(data)
    def __iter__(self):
        return self
    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]
    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

it = Reverse([1,2,3,4])

print(it.next())
print(it.next())
print(it.next())
print(it.next())
print(it.next())
