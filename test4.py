__author__ = 'darkerow'

# декоратор шаблон проектирования позволяющий
# добавить новое поведение к объекту

# Что такое декораторы? Для чего они нужны? Напишите кеширующий
# декоратор, который можно применить к функции get_long_response
# и который, если результат уже есть в кеше CACHE, вернет его, а
# если нет, то закеширует результат выполнения функции в CACHE и
# вернет его (результат). Результат выполнения функции
# get_long_response уникален для ее параметра user_id.

class Cache(object):
    __cache = {}
   # __slots__ = '__cache',

    @classmethod
    def get(cls, key):
        print('get', key)
        return cls.__cache[key]

    @classmethod
    def set(cls, key, value):
        print('set', key, value)
        cls.__cache[key] = value

    @classmethod
    def has(cls, key):
        print('has', key)
        return key in cls.__cache

CACHE = Cache()

def cache_decorator(func):
    def wrapped(uid):
        if CACHE.has(uid):
            return CACHE.get(uid)
        else:
            new_value = func(uid)
            CACHE.set(uid, new_value)
            return new_value

    return wrapped

@cache_decorator
def get_long_response(user_id):
    return user_id * 1000

print(get_long_response(12))
print()
print(get_long_response(15))
print()
print(get_long_response(12))
print()

