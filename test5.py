__author__ = 'darkerow'

# Вопрос на знание стандартной библиотеки питона и алгоритмического мышления (уровень Гуру).
# На входе - артикул (формат: последовательности цифр и букв разделены точкой или пробелом).
# необходимо заменить точки на пробелы и пробелы на точки с учетом всех возможных комбинаций.
# Например, на входе артикул
# 1.2.3
# На выходе должно быть:
# 1.2.3
# 1 2.3
# 1.2 3
# 1 2 3

def func(num, pos):
    return (num & (1 << pos)) >> pos

for pos in range(5):
    print(func(16, pos))

test = list("1.2.3")
testlen = len(test)
testlendiv = int(len(test)/2)
num = 0

while num < 2**testlendiv:
    counter = 0
    for i in range(1, testlen, 2):
        ch = func(num, counter)
        if ch == 0:
            ch = ' '
        else:
            ch = '.'
        test[i] = str(ch)
        counter += 1
    print(''.join(test))
    num+=1